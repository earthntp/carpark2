package com.example.carpark

import android.content.Context
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.databinding.DataBindingUtil
import com.example.carpark.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private  lateinit var binding: ActivityMainBinding
    var indexSlot:Int = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        setDefault()
        slot1()
        binding.apply {
            doneButton.setOnClickListener {
                add(it,indexSlot)
            }
            delButton.setOnClickListener {
                delete(it,indexSlot)
            }
            slot1Button.setOnClickListener {
                indexSlot = 1
                slot1()
            }
            slot2Button.setOnClickListener {
                indexSlot = 2
                slot2()
            }
            slot3Button.setOnClickListener {
                indexSlot = 3
                slot3()
            }
        }
    }
    private fun setDefault(){
        binding.apply {
            val car : Car = Car("","","")
            val car2 : Car2 = Car2("","","")
            val car3 : Car3 = Car3("","","")
            binding.car = car
            binding.car2 = car2
            binding.car3 = car3
        }
    }
    private fun add(view: View, int: Int){
        binding.apply {
            when(int){
                1 -> {
                    car?.License = editLicense.text.toString()
                    car?.Brand = editBrand.text.toString()
                    car?.Name = editOwner.text.toString()
                    slot1Button.text = editLicense.text
                    //slot1Button.setBackgroundColor(Color.RED)
                    invalidateAll()
                }
                2 -> {
                    car2?.License2 = editLicense.text.toString()
                    car2?.Brand2 = editBrand.text.toString()
                    car3?.Name3 = editOwner.text.toString()
                    slot2Button.text = editLicense2.text
                    //slot2Button.setBackgroundColor(Color.RED)
                    invalidateAll()
                }
                3 -> {
                    car3?.License3 = editLicense.text.toString()
                    car3?.Brand3 = editBrand.text.toString()
                    car3?.Name3 = editOwner.text.toString()
                    slot3Button.text = editLicense3.text
                    //slot3Button.setBackgroundColor(Color.RED)
                    invalidateAll()
                }
            }
            invalidateAll()
            val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)

        }
    }
    private fun delete(view: View, int: Int){
        binding.apply {
            when(int){
                1 -> {
                    editLicense.text.clear()
                    editBrand.text.clear()
                    editOwner.text.clear()
                    slot1Button.text = "Empty"
                    //slot1Button.setBackgroundColor(Color.GREEN)
                }
                2 -> {
                    editLicense2.text.clear()
                    editBrand2.text.clear()
                    editOwner2.text.clear()
                    slot2Button.text = "Empty"
                    //slot2Button.setBackgroundColor(Color.GREEN)
                }
                3 -> {
                    editLicense3.text.clear()
                    editBrand3.text.clear()
                    editOwner3.text.clear()
                    slot3Button.text = "Empty"
                    //slot3Button.setBackgroundColor(Color.GREEN)
                }
            }
        }
    }

    private fun slot1(){
        binding.apply {
            editLicense.visibility = View.VISIBLE
            editBrand.visibility = View.VISIBLE
            editOwner.visibility = View.VISIBLE
            slot1Button.setBackgroundColor(Color.GRAY)
            slot2Button.setBackgroundColor(Color.LTGRAY)
            slot3Button.setBackgroundColor(Color.LTGRAY)
            invis2()
            invis3()
        }
    }
    private fun slot2(){
        binding.apply {
            editLicense2.visibility = View.VISIBLE
            editBrand2.visibility = View.VISIBLE
            editOwner2.visibility = View.VISIBLE
            slot2Button.setBackgroundColor(Color.GRAY)
            slot1Button.setBackgroundColor(Color.LTGRAY)
            slot3Button.setBackgroundColor(Color.LTGRAY)
            invis1()
            invis3()
        }
    }
    private fun slot3(){
        binding.apply {
            editLicense3.visibility = View.VISIBLE
            editBrand3.visibility = View.VISIBLE
            editOwner3.visibility = View.VISIBLE
            slot3Button.setBackgroundColor(Color.GRAY)
            slot2Button.setBackgroundColor(Color.LTGRAY)
            slot1Button.setBackgroundColor(Color.LTGRAY)
            invis1()
            invis2()
        }
    }
    private fun invis1(){
        editLicense.visibility = View.INVISIBLE
        editBrand.visibility = View.INVISIBLE
        editOwner.visibility = View.INVISIBLE
    }
    private fun invis2(){
        editLicense2.visibility = View.INVISIBLE
        editBrand2.visibility = View.INVISIBLE
        editOwner2.visibility = View.INVISIBLE
    }
    private fun invis3(){
        editLicense3.visibility = View.INVISIBLE
        editBrand3.visibility = View.INVISIBLE
        editOwner3.visibility = View.INVISIBLE
    }
}

